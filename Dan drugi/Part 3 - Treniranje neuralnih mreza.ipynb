{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Treniranje neuronskih mreža\n",
    "\n",
    "Mreža koju smo formirali u prethodnom delu nije previše pametna, ona ne zna ništa o našim ručno pisanim brojevima. Neuronske mreže sa nelinearnim aktivacijama funkcionišu kao univerzalni aproksimatori funkcija. Postoji neka funkcija koja mapira vaš ulaz na izlaz. Na primer, slike ručno napisanih brojeva na verovatnoće klasa. Moć neuronskih mreža je u tome što ih možemo obučiti da aproksimiraju ovu funkciju, i u osnovi bilo koju funkciju, ako imamo dovoljno podataka i vremena za računanje.\n",
    "\n",
    "<img src=\"assets/function_approx.png\" width=500px>\n",
    "\n",
    "Na početku, mreža je naivna, ona ne poznaje funkciju koja mapira ulaze na izlaze. Obučavamo mrežu tako što joj pokazujemo primere stvarnih podataka, a zatim podešavamo parametre mreže tako da aproksimira ovu funkciju.\n",
    "\n",
    "Da bismo pronašli ove parametre, potrebno nam je da znamo koliko loše mreža predviđa stvarne izlaze. Za to računamo **funkciju greške** (takođe nazvanu loss), meru naše greške predikcije. Na primer, srednji kvadratna greška često se koristi u problemima regresije i binarne klasifikacije\n",
    "\n",
    "$$\n",
    "\\large \\ell = \\frac{1}{2n}\\sum_i^n{\\left(y_i - \\hat{y}_i\\right)^2}\n",
    "$$\n",
    "\n",
    "gde je $n$ broj trening primera, $y_i$ su stvarne oznake, a $\\hat{y}_i$ su predviđene oznake.\n",
    "\n",
    "Minimizacijom ove greške u odnosu na parametre mreže, možemo pronaći konfiguracije gde je greška minimalna, a mreža može tačno da predvidi ispravne oznake sa visokom tačnošću. Ovaj minimum nalazimo korišćenjem procesa koji se zove **gradijentni spust - gradient descent**. Gradijent je nagib funkcije greške i pokazuje u pravcu najbrže promene. Da bismo stigli do minimuma u što kraćem vremenu, želimo da pratimo gradijent (nadole). Možete zamisliti ovaj proces kao silaženje sa planine prateći najstrmiju padinu ka osnovi.\n",
    "\n",
    "<img src='assets/gradient_descent.png' width=350px>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algoritam propagacije unazad\n",
    "\n",
    "Za jednoslojne mreže, gradijentni spust je jednostavan za implementaciju. Međutim, to je komplikovanije za dublje, višeslojne neuronske mreže poput one koju smo izgradili. Dovoljno je komplikovano da je trebalo oko 30 godina istraživačima da shvate kako da obuče višeslojne mreže.\n",
    "\n",
    "Obuka višeslojnih mreža se vrši kroz **propagaciju unazad** koja je zapravo samo primena pravila o lancu izračunavanja iz računarske analize. Najlakše je razumeti ako pretvorimo dvoslojnu mrežu u grafički prikaz.\n",
    "\n",
    "<img src='assets/backprop_diagram.png' width=550px>\n",
    "\n",
    "Prilikom prolaska unapred kroz mrežu, naši podaci i operacije idu od dna prema vrhu. Ulaz $x$ prolazi kroz linearnu transformaciju $L_1$ sa težinama $W_1$ i bias-ima $b_1$. Izlaz zatim prolazi kroz sigmoidnu operaciju $S$ i još jednu linearnu transformaciju $L_2$. Na kraju računamo grešku $\\ell$. Koristimo grešku kao meru koliko su loše predikcije mreže. Cilj je zatim prilagoditi težine i bias-eve tako da minimiziramo grešku.\n",
    "\n",
    "Da bismo obučili težine gradijentnim spustom, propagiramo gradijent greške unazad kroz mrežu. Svaka operacija ima određeni gradijent između ulaza i izlaza. Dok šaljemo gradijente unazad, množimo dolazni gradijent sa gradijentom za operaciju. Matematički, ovo je zapravo samo računanje gradijenta greške u odnosu na težine korišćenjem pravila o lancu izračunavanja.\n",
    "\n",
    "$$\n",
    "\\large \\frac{\\partial \\ell}{\\partial W_1} = \\frac{\\partial L_1}{\\partial W_1} \\frac{\\partial S}{\\partial L_1} \\frac{\\partial L_2}{\\partial S} \\frac{\\partial \\ell}{\\partial L_2}\n",
    "$$\n",
    "\n",
    "Ažuriramo naše težine koristeći ovaj gradijent sa nekom stopom učenja $\\alpha$. \n",
    "\n",
    "$$\n",
    "\\large W^\\prime_1 = W_1 - \\alpha \\frac{\\partial \\ell}{\\partial W_1}\n",
    "$$\n",
    "\n",
    "Stopa učenja $\\alpha$ se postavlja tako da su koraci ažuriranja težina dovoljno mali da se iterativna metoda smiri u minimumu."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Losses u PyTorch-u\n",
    "\n",
    "Jedan od načina izračunavanja greške je (`nn.CrossEntropyLoss`). \n",
    "\n",
    "Pogledaj [dokumentaciju za `nn.CrossEntropyLoss`](https://pytorch.org/docs/stable/nn.html#torch.nn.CrossEntropyLoss),\n",
    "\n",
    "> Ovaj kriterijum kombinuje `nn.LogSoftmax()` i `nn.NLLLoss()` u jednoj klasi.\n",
    "\n",
    "To znači da treba da prosledimo sirove izlaze naše mreže u računanje greške, a ne izlaz softmax funkcije."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The MNIST datasets are hosted on yann.lecun.com that has moved under CloudFlare protection\n",
    "# Run this script to enable the datasets download\n",
    "# Reference: https://github.com/pytorch/vision/issues/1938\n",
    "\n",
    "from six.moves import urllib\n",
    "opener = urllib.request.build_opener()\n",
    "opener.addheaders = [('User-agent', 'Mozilla/5.0')]\n",
    "urllib.request.install_opener(opener)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "import torch.nn.functional as F\n",
    "from torchvision import datasets, transforms\n",
    "\n",
    "# Define a transform to normalize the data\n",
    "transform = transforms.Compose([transforms.ToTensor(),\n",
    "                                transforms.Normalize((0.5,), (0.5,)),\n",
    "                              ])\n",
    "# Download and load the training data\n",
    "trainset = datasets.MNIST('~/.pytorch/MNIST_data/', download=True, train=True, transform=transform)\n",
    "trainloader = torch.utils.data.DataLoader(trainset, batch_size=64, shuffle=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a feed-forward network\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10))\n",
    "\n",
    "# Define the loss\n",
    "criterion = nn.CrossEntropyLoss()\n",
    "\n",
    "# Get our data\n",
    "dataiter = iter(trainloader)\n",
    "\n",
    "images, labels = next(dataiter)\n",
    "\n",
    "# Flatten images\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "# Forward pass, get our logits\n",
    "logits = model(images)\n",
    "# Calculate the loss with the logits and the labels\n",
    "loss = criterion(logits, labels)\n",
    "\n",
    "print(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Drugi način je: izgraditi model koji vraća logaritamski softmax kao izlaz koristeći `nn.LogSoftmax` ili `F.log_softmax` ([dokumentacija](https://pytorch.org/docs/stable/nn.html#torch.nn.LogSoftmax)). Zatim dobiti stvarne verovatnoće uzimanjem eksponencijalne funkcije `torch.exp(output)`. Sa izlazom logaritamskog softmaxa, želite koristiti grešku negativne logaritamske verovatnoće, `nn.NLLLoss` ([dokumentacija](https://pytorch.org/docs/stable/nn.html#torch.nn.NLLLoss)).\n",
    "\n",
    ">**Vežba:** Napravite model koji vraća logaritamski softmax kao izlaz i izračunajte grešku koristeći `nn.NLLLoss`. Imajte na umu da za `nn.LogSoftmax` i `F.log_softmax` morate pravilno postaviti argument `dim`. `dim=0` računa softmax preko redova, tako da svaka kolona sumira na 1, dok `dim=1` računa preko kolona tako da svaki red sumira na 1. Razmislite o tome šta želite da bude izlaz i odaberite `dim` odgovarajuće."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: Build a feed-forward network\n",
    "model = \n",
    "\n",
    "# TODO: Define the loss\n",
    "criterion = \n",
    "\n",
    "### Run this to check your work\n",
    "# Get our data\n",
    "dataiter = iter(trainloader)\n",
    "\n",
    "images, labels = next(dataiter)\n",
    "\n",
    "# Flatten images\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "# Forward pass, get our logits\n",
    "logits = model(images)\n",
    "# Calculate the loss with the logits and the labels\n",
    "loss = criterion(logits, labels)\n",
    "\n",
    "print(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Autograd\n",
    "\n",
    "Sada kada znamo kako izračunati grešku, kako je koristimo za izvođenje propagacije unazad? Torch pruža modul `autograd` za automatsko računanje gradijenata tenzora. Možemo ga koristiti da izračunamo gradijente svih naših parametara u odnosu na grešku. Da bismo osigurali da PyTorch prati operacije na tenzoru i računa gradijente, potrebno je postaviti `requires_grad = True` na tenzoru. To možete učiniti prilikom kreiranja sa ključnom reči `requires_grad`, ili bilo kada sa `x.requires_grad_(True)`.\n",
    "\n",
    "Možete isključiti gradijente za blok koda koristeći `torch.no_grad()`:\n",
    "```python\n",
    "x = torch.zeros(1, requires_grad=True)\n",
    ">>> with torch.no_grad():\n",
    "...     y = x * 2\n",
    ">>> y.requires_grad\n",
    "False\n",
    "```\n",
    "\n",
    "Takođe, možete uključiti ili isključiti gradijente u celosti sa `torch.set_grad_enabled(True|False)`.\n",
    "\n",
    "Gradijenti se računaju u odnosu na neku promenljivu `z` pomoću `z.backward()`. Ovo vrši propagaciju unazad kroz operacije definisane u `z`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = torch.tensor(3.0, requires_grad=True)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = x**2\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## grad_fn shows the function that generated this variable\n",
    "print(y.grad_fn)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(x.grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y.backward()\n",
    "print(x.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ovi proračuni gradijenata su posebno korisni za neuronske mreže. Za treniranje, potrebni su nam gradijenti greške u odnosu na težine. Sa PyTorch-om, pokrećemo podatke unapred kroz mrežu da bismo izračunali grešku, a zatim idemo unazad da bismo izračunali gradijente u odnosu na grešku. Kada imamo gradijente, možemo napraviti korak gradijentnog spuštanja."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loss i Autograd zajedno"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a feed-forward network\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10),\n",
    "                      nn.LogSoftmax(dim=1))\n",
    "\n",
    "criterion = nn.NLLLoss()\n",
    "dataiter = iter(trainloader)\n",
    "images, labels = next(dataiter)\n",
    "images = images.view(images.shape[0], -1)\n",
    "\n",
    "logits = model(images)\n",
    "loss = criterion(logits, labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Before backward pass: \\n', model[0].weight.grad)\n",
    "\n",
    "loss.backward()\n",
    "\n",
    "print('After backward pass: \\n', model[0].weight.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Treniranje mreže\n",
    "\n",
    "Preostao je još jedan deo koji nam je potreban za početak treniranja, a to je optimizator koji ćemo koristiti za ažuriranje težina sa gradijentima. Optimizatore dobijamo iz PyTorch-ovog [`optim` paketa](https://pytorch.org/docs/stable/optim.html). Na primer, možemo koristiti stohastičko gradijentno spuštanje sa `optim.SGD`. Ispod možete videti kako da definišete optimizator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from torch import optim\n",
    "\n",
    "# Optimizers require the parameters to optimize and a learning rate\n",
    "optimizer = optim.SGD(model.parameters(), lr=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generalni proces sa PyTorch-om je:\n",
    "\n",
    "* Prođite unapred kroz mrežu\n",
    "* Koristite izlaz mreže za izračunavanje greške\n",
    "* Izvršite prolaz unazad kroz mrežu sa `loss.backward()` da biste izračunali gradijente\n",
    "* Napravite korak sa optimizatorom da ažurirate težine\n",
    "\n",
    "Obratite pažnju na liniju koda `optimizer.zero_grad()`. Kada radite više prolaza unazad sa istim parametrima, gradijenti se akumuliraju. To znači da morate resetovati gradijente pri svakom prolazu treniranja ili ćete zadržati gradijente iz prethodnih batch-ova treniranja."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Initial weights - ', model[0].weight)\n",
    "\n",
    "dataiter = iter(trainloader)\n",
    "images, labels = next(dataiter)\n",
    "images.resize_(64, 784)\n",
    "\n",
    "# Clear the gradients, do this because gradients are accumulated\n",
    "optimizer.zero_grad()\n",
    "\n",
    "# Forward pass, then backward pass, then update weights\n",
    "output = model(images)\n",
    "loss = criterion(output, labels)\n",
    "loss.backward()\n",
    "print('Gradient -', model[0].weight.grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Take an update step and view the new weights\n",
    "optimizer.step()\n",
    "print('Updated weights - ', model[0].weight)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sada ćemo staviti ovaj algoritam u \"for\" petlju kako bismo prošli kroz sve slike. Jedan prolaz kroz ceo dataset naziva se *epohom*. Dakle, ovde ćemo proći kroz `trainloader` kako bismo dobili naše trenirane batch-ove. Za svaki batch, radićemo prolaz treniranja gde ćemo izračunati grešku, uraditi prolaz unazad i ažurirati težine.\n",
    "\n",
    ">**Zadatak:** Implementirajte proces treniranja naše mreže. Ako ste ga ispravno implementirali, trebalo bi da vidite kako grešku pri treniranju opada sa svakom epohom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your solution here\n",
    "\n",
    "model = nn.Sequential(nn.Linear(784, 128),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(128, 64),\n",
    "                      nn.ReLU(),\n",
    "                      nn.Linear(64, 10),\n",
    "                      nn.LogSoftmax(dim=1))\n",
    "\n",
    "criterion = nn.NLLLoss()\n",
    "optimizer = optim.SGD(model.parameters(), lr=0.003)\n",
    "\n",
    "epochs = 5\n",
    "for e in range(epochs):\n",
    "    running_loss = 0\n",
    "    for images, labels in trainloader:\n",
    "        # Flatten MNIST images into a 784 long vector\n",
    "        images = images.view(images.shape[0], -1)\n",
    "    \n",
    "        # TODO: Training pass\n",
    "        \n",
    "        loss = \n",
    "        \n",
    "        running_loss += loss.item()\n",
    "    else:\n",
    "        print(f\"Training loss: {running_loss/len(trainloader)}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import helper\n",
    "\n",
    "dataiter = iter(trainloader)\n",
    "images, labels = next(dataiter)\n",
    "\n",
    "img = images[0].view(1, 784)\n",
    "# Turn off gradients to speed up this part\n",
    "with torch.no_grad():\n",
    "    logps = model(img)\n",
    "\n",
    "# Output of the network are log-probabilities, need to take exponential for probabilities\n",
    "ps = torch.exp(logps)\n",
    "helper.view_classify(img.view(1, 28, 28), ps)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
